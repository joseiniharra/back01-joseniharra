package com.bbva.apirest;

import com.bbva.apirest.controller.ProductoController;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class EntregableBack01ApplicationTests {

	@Test
	public void contextLoads() {
		ProductoController hmController = new ProductoController();
		assertEquals("Hola soy Jose Niharra", hmController.saludar("Jose Niharra"));
		assertEquals("Hola soy Javier Pérez", hmController.saludar("Javier Pérez"));
	}

}


