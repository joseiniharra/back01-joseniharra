package com.bbva.apirest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EntregableBack01Application {

	public static void main(String[] args) {
		SpringApplication.run(EntregableBack01Application.class, args);
	}

}
