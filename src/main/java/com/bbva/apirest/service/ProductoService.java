package com.bbva.apirest.service;

import com.bbva.apirest.model.ProductoModel;
import com.bbva.apirest.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service public class ProductoService {
    @Autowired
    ProductoRepository productoRepository;


    public List<ProductoModel> findAll(){
        return productoRepository.findAll();
    }

    public Optional<ProductoModel> findById(String id){
        return productoRepository.findById(id);
    }

    public ProductoModel save(ProductoModel entity){
        return productoRepository.save(entity);
    }

    public boolean delete(ProductoModel entity) {
        try {
            productoRepository.delete(entity);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public boolean existById(String id){
        return productoRepository.existsById(id);
    }

    public boolean deleteByID(String id){
        productoRepository.deleteById(id);
        return true;
    }

}
