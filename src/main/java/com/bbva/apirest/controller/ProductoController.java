package com.bbva.apirest.controller;

import com.bbva.apirest.model.ProductoModel;
import com.bbva.apirest.model.ProductoPrecio;
import com.bbva.apirest.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v3")

public class ProductoController {

    @Autowired
    ProductoService productoService;

// Consulta todos los elementos
    @GetMapping("/productos")
    public ResponseEntity<List<ProductoModel>> getProductos(){
       if (productoService.findAll().isEmpty()){
            ArrayList<ProductoModel> noProducto = new ArrayList<ProductoModel>();
            return new ResponseEntity<>(noProducto,HttpStatus.NO_CONTENT);
       } else{
            return new ResponseEntity<>(productoService.findAll(),HttpStatus.OK);
       }
    }
//  Consulta elemento por id

    @GetMapping("/productos/{id}")
    public Optional<ProductoModel> getProductoId(@PathVariable String id){
            return productoService.findById(id);
        }

//  Creación de 1 elemento

    @PostMapping("/productos")
    public ResponseEntity<ProductoModel> postProducto(@RequestBody ProductoModel newProducto){
        ProductoModel noProducto;
        if (productoService.existById(newProducto.getId())){
            noProducto = new ProductoModel();
            return new ResponseEntity<>(noProducto, HttpStatus.IM_USED);
        }
        else{
            productoService.save(newProducto);
            return new ResponseEntity<>(newProducto, HttpStatus.CREATED);
        }
    }

//  Actualización total elemento

    @PutMapping("/productos")
    public ResponseEntity<ProductoModel> putProducto(@RequestBody ProductoModel updateProducto){
        ProductoModel noProducto;
        if (productoService.existById(updateProducto.getId())){

            productoService.save(updateProducto);
            return new ResponseEntity<>(updateProducto,HttpStatus.OK);
        }
        else {
            noProducto = new ProductoModel();
            return new ResponseEntity<>(noProducto, HttpStatus.NOT_FOUND);
        }
    }

// Borrado de un elemento sin id por parámetro
    @DeleteMapping("/productos")
    public ResponseEntity<Boolean> deleteProduto(@RequestBody ProductoModel productoToDelete){
        if (productoService.existById(productoToDelete.getId())) {
            productoService.delete(productoToDelete);
            return new ResponseEntity<>(true, HttpStatus.OK);
        } else{
            return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
        }
    }

// Borrado con id por parámetro

    @DeleteMapping("/productos/{id}")
    public ResponseEntity <Boolean> deleteProductoId(@PathVariable String id){
        if (productoService.existById(id)) {
            productoService.deleteByID(id);
            return new ResponseEntity<>(true, HttpStatus.OK);
        } else{
            return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
        }
    }

// Actualización parcial de Precio

    @PatchMapping("/productos")

    public ResponseEntity<ProductoModel> patchProducto(@RequestBody ProductoPrecio productoPrecio){
        Optional <ProductoModel> productoRec = productoService.findById(productoPrecio.getId());
        ProductoModel productoAux;

        if (productoRec.isPresent()){
            productoAux = productoRec.get();
            productoAux.setPrecio(productoPrecio.getPrecio());
            productoService.save(productoAux);
            return new ResponseEntity<>(productoAux,HttpStatus.OK);

        }  else {
            productoAux = new ProductoModel();
            return new ResponseEntity<>(productoAux, HttpStatus.NOT_FOUND);
        }

    }


// Este método no es parte del entregable,
// pero se mete para probar los Test
    @GetMapping("/saludos")
    public String saludar(String NombreApell){
        return("Hola soy " + NombreApell);
    }

}


